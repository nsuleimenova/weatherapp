//
//  ViewController.swift
//  lecture8
//
//  Created by admin on 08.02.2021.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var feelsLikeTemp: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectCityButton: UIButton!
    
    let Cities : [String: String] = ["Astana":"lat=51.180&lon=71.446",
                                            "Karagandy" : "lat=49.804&lon=73.109",
                                            "Almaty" : "lat=43.238&lon=76.889"]
    
    
    var url = Constants.host
    var myData: Model?
    
    
    private var decoder: JSONDecoder = JSONDecoder()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CollectionViewCell.nib, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        tableView.register(TableViewCell.nib, forCellReuseIdentifier: TableViewCell.identifier)
        fetchData()
        cityName.text = "Astana"
    }
    
    
    func updateUI(){
//        cityName.text = "Astana"
        temp.text = "\(myData?.current?.temp ?? 0.0) °C"
        feelsLikeTemp.text = "\(myData?.current?.feels_like ?? 0.0) °C"
        desc.text = myData?.current?.weather?.first?.description
        // MARK: FOR UPDATE TABLE AND COLLECTION VIEW
        collectionView.reloadData()
        tableView.reloadData()
        
    }
    
    func fetchData(){
        AF.request(url).responseJSON { (response) in
            switch response.result{
            case .success(_):
                guard let data = response.data else { return }
                guard let answer = try? self.decoder.decode(Model.self, from: data) else{ return }
                self.myData = answer
                self.updateUI()
            case .failure(let err):
                print(err.errorDescription ?? "")
            }
        }
    }
    
    
    @IBAction func selectCityTAp(_ sender: Any) {
        let alert = UIAlertController(title: "Please select a city", message: nil, preferredStyle: .alert)
        

       

        alert.addAction(UIAlertAction(title: "Almaty", style: .default, handler: {_ in
            
            self.cityName.text = "Almaty"
            
            self.url = "https://api.openweathermap.org/data/2.5/onecall?\(self.Cities["Almaty"] ?? "lat=51.1801&lon=71.446")&exclude=minutely,alerts&appid=0e9477e67e53c8c91844f7d87860ae02&units=metric"
            self.fetchData()
        }))
        

        alert.addAction(UIAlertAction(title: "Karagandy", style: .default, handler: {_ in
            self.cityName.text = "Karagandy"
            self.url = "https://api.openweathermap.org/data/2.5/onecall?\(self.Cities["Astana"] ?? "lat=51.1801&lon=71.446")&exclude=minutely,alerts&appid=0e9477e67e53c8c91844f7d87860ae02&units=metric"
            self.fetchData()
        }))
        
        alert.addAction(UIAlertAction(title: "Astana", style: .default, handler: {_ in
            self.cityName.text = "Astana"
            self.url = "https://api.openweathermap.org/data/2.5/onecall?\(self.Cities["Karagandy"] ?? "lat=51.1801&lon=71.446")&exclude=minutely,alerts&appid=0e9477e67e53c8c91844f7d87860ae02&units=metric"
            self.fetchData()
        }))
        
        
        
        
        

        self.present(alert, animated: true)
    }
    
    
    
}


// MARK: TABLEVIEW
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myData?.daily?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        cell.descLabel.text = myData?.daily?[indexPath.row].weather?.first?.description
        cell.tempLabel.text = "\(myData?.daily?[indexPath.row].temp?.day ?? 0) / \(myData?.daily?[indexPath.row].feels_like?.day ?? 0) °C"
        
        print(indexPath.row)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        let day = formatter.string(from: date)
        formatter.dateFormat = "MM"
        let month = formatter.string(from: date)
        cell.dateLabel.text = String(Int(day)! + indexPath.row) + "." + month
        
        return cell
    }
    
    
    
    
    
}


// MARK: COLLECTIONVIEW

extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myData?.hourly?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        let item = myData?.hourly?[indexPath.item]
        cell.temp.text = "\(item?.temp ?? 0.0) °C"
        cell.feelsLike.text = "\(item?.feels_like ?? 0.0) °C"
        cell.desc.text = item?.weather?.first?.description
        return cell

    }
    
    
}

extension ViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}


